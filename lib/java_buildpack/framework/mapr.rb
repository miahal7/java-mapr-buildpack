# frozen_string_literal: true

# Cloud Foundry Java Buildpack
# Copyright 2013-2018 the original author or authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'java_buildpack/component/base_component'
require 'java_buildpack/framework'

module JavaBuildpack
  module Framework

    # Encapsulates the functionality for enabling zero-touch Mapr support.
    class Mapr < JavaBuildpack::Component::BaseComponent

      def detect
        "mapr=mapr-v6.0-1234"
      end

      # (see JavaBuildpack::Component::BaseComponent#compile)
      def compile
        # download_jar
        @droplet.copy_resources "#{@droplet.root}/deps"
        return
      end

      # (see JavaBuildpack::Component::BaseComponent#release)
      def release

      end
    end
  end
end
