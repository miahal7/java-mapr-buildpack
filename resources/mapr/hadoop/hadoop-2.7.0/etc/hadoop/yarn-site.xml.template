<?xml version="1.0"?>
<!--
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License. See accompanying LICENSE file.
-->
<configuration>
  <!-- Site specific YARN configuration properties -->
  <property>
      <name>yarn.resourcemanager.hostname</name>
      <value>__RM_IP__</value>
      <description>host is the hostname of the resourcemanager</description>
  </property>
  <property>
    <name>yarn.resourcemanager.scheduler.address</name>
    <value>${yarn.resourcemanager.hostname}:8030</value>
    <description>host is the hostname of the resourcemanager and port is the port
      on which the Applications in the cluster talk to the Resource Manager.
    </description>
  </property>

  <property>
    <name>yarn.resourcemanager.resource-tracker.address</name>
    <value>${yarn.resourcemanager.hostname}:8031</value>
  </property>

  <property>
    <name>yarn.resourcemanager.address</name>
    <value>${yarn.resourcemanager.hostname}:8032</value>
    <description>the host is the hostname of the ResourceManager and the port is the port on which the clients can talk
      to the Resource Manager.
    </description>
  </property>

  <property>
    <description>Amount of physical memory, in MB, that can be allocated
    for containers.</description>
    <name>yarn.nodemanager.resource.memory-mb</name>
    <value>${nodemanager.resource.memory-mb}</value>
  </property>

  <property>
    <description>Number of CPU cores that can be allocated
    for containers.</description>
    <name>yarn.nodemanager.resource.cpu-vcores</name>
    <value>${nodemanager.resource.cpu-vcores}</value>
  </property>

  <property>
    <description>Number of spindles that can be allocated
    for containers.</description>
    <name>yarn.nodemanager.resource.io-spindles</name>
    <value>${nodemanager.resource.io-spindles}</value>
  </property>

  <property>
    <description>who will execute(launch) the containers.</description>
    <name>yarn.nodemanager.container-executor.class</name>
    <value>org.apache.hadoop.yarn.server.nodemanager.LinuxContainerExecutor</value>
  </property>

  <property>
    <name>yarn.nodemanager.aux-services</name>
    <value>mapreduce_shuffle,mapr_direct_shuffle</value>
    <description>shuffle service that needs to be set for Map Reduce to run</description>
  </property>

  <property>
    <name>yarn.nodemanager.aux-services.mapreduce_shuffle.class</name>
    <value>org.apache.hadoop.mapred.ShuffleHandler</value>
  </property>

  <property>
    <name>yarn.nodemanager.aux-services.mapr_direct_shuffle.class</name>
    <value>com.mapr.hadoop.mapred.LocalVolumeAuxService</value>
  </property>

  <property>
    <name>mapreduce.job.shuffle.provider.services</name>
    <value>mapr_direct_shuffle</value>
  </property>

  <!-- BEGIN: Logging related settings -->
  <property>
    <name>yarn.log-aggregation-enable</name>
    <value>false</value>
    <description>Determines if log aggregation should be enabled. When it is enabled, logs for map reduce jobs
      will be aggregated using this method only if yarn.use-central-logging-for-mapreduce-only is set to false.</description>
  </property>

  <property>
    <name>yarn.use-central-logging-for-mapreduce-only</name>
    <value>true</value>
    <description>Determines if MapR central logging should be enabled. This is only applicable for map reduce jobs.
      So to keep logs in MapRFS for other applications, enable log aggregation.</description>
  </property>

  <property>
    <name>yarn.log-aggregation.retain-seconds</name>
    <value>2592000</value>
    <description>Determines how long to keep the logs in MapRFS. Defaults to 30 days if not specified.</description>
  </property>

  <property>
    <name>yarn.log-aggregation.retain-check-interval-seconds</name>
    <value>-1</value>
    <description>Determines how often to perform log retention checks. When set to the default value of -1, the check time
      will be set to 1/10th of the retention time. So it will be 3 days when retention time is 30 days.</description>
  </property>
  <!--  END: Logging related settings -->

</configuration>
