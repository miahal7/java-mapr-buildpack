# Copyright (c) 2009 & onwards. MapR Tech, Inc., All rights reserved

# Check whether there is already same script is running or not.
# if there is already running then exit.
CheckForSingleInstance() {
  curProcName=`basename "$0"`;
  lock="/tmp/$curProcName.lck"
  exec 7>$lock;

  # Mac has no flock
  which flock >/dev/null 2>&1
  if [[ $? -eq 0 ]]; then
    if ! flock -n -x 7;
    then
      echo "$0 is already running. Exiting $$";
      exit 1
    fi
  fi
}

CheckForSingleInstanceWithTimeout() {
  curProcName=`basename "$0"`;
  lock="/tmp/$curProcName.lck"
  exec 7>$lock;

  # Mac has no flock
  which flock >/dev/null 2>&1
  if [[ $? -eq 0 ]]; then
    if ! flock -x -w $1 7;
    then
      echo "$0 is already running. Exiting $$";
      exit 1
    fi
  fi
}

# Promps the user that an error has occued and forces them to continue with y or n
function PromptUserOnError() {
    counter=3 # Times to iterate through error message
    if [ $promptStyle == "y" ]; then
        echo "Continuing automatically"
        return
    fi
    if [ $promptStyle == "n" ]; then
        counter=-1
        echo "Prompt evaluated to no automatically... Exiting"
    fi
    # Iterate through question three times to prevent infinite loop
    while [ $counter -gt 0 ]; do
        let counter=counter-1 
        read -p "Do you still wish to continue? [y/n] " continueAnswer
        case $continueAnswer in
            [Yy]* ) return;; # Return on y, skips rest of file
            [Nn]* ) echo "Aborted by user"
                break;;
                * ) echo "Please answer y or n.";;
        esac
    done
    # If it reached here, then exit script
    if [ $counter -eq 0 ]; then
        # Counter was decreased to 0 after 3 tries
        echo "No sutible answer recieved after 3 tries. Automatically exiting script"
    fi
    # Clean disk file
    CleanupDiskFile
    ExitSingleInstance 1
}

#Prints if verbose is 1
function VerboseEcho() {
    if [[ ! -z $verboseOn && $verboseOn -ne 0 ]]; then
        echo $1
    fi
}

ExitSingleInstance() {
  # Mac has no flock
  which flock >/dev/null 2>&1
  if [[ $? -eq 0 ]]; then
    flock -u 7;
    rm -f $lock
  fi
  exit $1
}

UnlockSingleInstance() {
  # Mac has no flock
  which flock >/dev/null 2>&1
  if [[ $? -eq 0 ]]; then
    flock -u 7;
  fi
}


CheckForRoot() {
  if [ $(/usr/bin/id -u) -ne 0 ]; then
    echo "ERROR: Not running as root"
    #logErr "Not running as root"
    exit 1
  fi 
}

function logHeader() {
  #DATE| TIME| HOSTNAME | PROCESS(PID) | COMPONENT | SEVERITY | TRIGGER | MESSAGE
  cmdName=`basename "$0"`;
  now=`date +%Y-%m-%d\ %H:%M:%S.$(( $(date +%-N) / 1000000 ))`
  logheader="$now $1 $cmdName($$) Install $2"
}

function logConsole () {
  local skipHeader=0
  case "$1" in
    "-hasHeader")
       skipHeader=1
       shift;
  esac
  if [ $skipHeader -eq 0 ]; then
    caller=( `caller 0` );
    line="${callInfo[1]}:${callInfo[0]}"
    logHeader $HOSTNAME $line ERROR
    echo "$logheader $*"
  else
    echo "$*"
  fi

}

function logIt () {
  if [ "$logFile" == "" ]; then
    echo $*;
  else 
    echo $* >> "$logFile";
  fi
}

# Added -tty, -both options to allow function to log to console/stdout
# or both console/stdout and log file, or just log file (no option)
function logInfo () {
  local ttyOutput=0
  local fileOutput=1
  case "$1" in
    "-both")
         ttyOutput=1;
         shift 1;;
     "-tty")
         ttyOutput=1;
         fileOutput=0;
         shift 1;;
  esac
  callInfo=( `caller 0` );
  line="${callInfo[1]}:${callInfo[0]}"
  logHeader `hostname` $line INFO
  if [ $fileOutput -eq 1 ]; then
    logIt "$logheader $*"
  fi
  if [ $ttyOutput -eq 1 ]; then
    logConsole -hasHeader "$logheader $*"
  fi
  VerboseEcho "$*"
}

function logErr () {
  local ttyOutput=0
  local fileOutput=1
  case "$1" in
    "-both")
         ttyOutput=1;
         shift 1;;
     "-tty")
         ttyOutput=1;
         fileOutput=0;
         shift 1;;
  esac
  caller=( `caller 0` );
  line="${callInfo[1]}:${callInfo[0]}"
  logHeader $HOSTNAME $line ERROR
  if [ $fileOutput -eq 1 ] ; then
    logIt "$logheader $*"
  fi
  if [ $ttyOutput -eq 1 ] ; then
    logConsole -hasHeader "$logheader $*"
  fi
}

function logWarn () {
  local ttyOutput=0
  local fileOutput=1
  case "$1" in
    "-both")
         ttyOutput=1;
         shift 1;;
     "-tty")
         ttyOutput=1;
         fileOutput=0;
         shift 1;;
  esac
  caller=( `caller 0` );
  line="${callInfo[1]}:${callInfo[0]}"
  logHeader $HOSTNAME $line WARN
  if [ $fileOutput -eq 1 ]; then
    logIt "$logheader $*"
  fi
  if [ $ttyOutput -eq 1 ]; then
    logConsole -hasHeader "$logheader $*"
  fi
}

function trace() {
  caller=( `caller 0` );
  line="${callInfo[1]}:${callInfo[0]}"
  logHeader $HOSTNAME $line TRACE
  logIt "$logheader $*"

  if [ "$logFile" == "" ]; then 
    $*
  else 
    $* >> "$logFile" 2>&1
  fi

  return $?;
}
