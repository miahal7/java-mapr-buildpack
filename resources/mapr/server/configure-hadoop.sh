#!/bin/bash
# Copyright (c) 2014 & onwards. MapR Tech, Inc., All rights reserved

INSTALL_DIR=${MAPR_HOME:=/opt/mapr}
SERVER_DIR=${INSTALL_DIR}/server
. ${SERVER_DIR}/configure-common.sh

# Source hadoop version variables
. ${INSTALL_DIR}/conf/hadoop_version

# Gloabal variables initialized in main method
HADOOP_VERSION=
HADOOP=
RM_IP=
TL_IP=
HS_IP=
HADOOP_HOME=
HADOOP_CORE_HOME=

FDLIMIT=32768
SYSCONF=/etc/sysctl.conf

RM_RESTART_FILE="${RESTART_DIR}/resourcemanager_restart.sh"
TL_RESTART_FILE="${RESTART_DIR}/timelineserver_restart.sh"
############## functions
ConfigureCommon() {
  # Remove old maprfs jars.
  cleanUpOldMapRfsJars ".*maprfs-[[:digit:]].*\.jar"
  cleanUpOldMapRfsJars ".*maprfs-jni-[[:digit:]].*\.jar"
  cleanUpOldMapRfsJars ".*maprfs-core-[[:digit:]].*\.jar"
  cleanUpOldMapRfsJars ".*mysql-container-java-[[:digit:]].*\.jar"
}

ConfigureYarnSiteXml() {
  local phatJar=`ls $INSTALL_DIR/lib/maprfs-*.jar | grep -v jni | grep -v diagnostic | grep -v core | grep -v test`
  local FILENAME="yarn-site"
  local FILE="${HADOOP2_HOME}/etc/hadoop/${FILENAME}.xml"
  local TEMP_FILE="${HADOOP2_HOME}/etc/hadoop/${FILENAME}.xml.tmp"
  local TIMESTAMP=$(date +%F.%H-%M)

  if [ -f ${FILE} ]; then
    logInfo "Backing up \"$HADOOP2_HOME/etc/hadoop/yarn-site.xml\" to \"$HADOOP2_HOME/etc/hadoop/yarn-site-${TIMESTAMP}.xml\""
    cp ${FILE} $HADOOP2_HOME/etc/hadoop/${FILENAME}-${TIMESTAMP}.xml
  fi

  $HADOOP2_HOME/bin/hadoop jar $phatJar $@ $HADOOP2_HOME/etc/hadoop/yarn-site.xml > $TEMP_FILE

  if [ $? -ne 0 ];then
    echo "ERROR configuring yarn-site.xml."
    exit 1
  fi
  mv ${TEMP_FILE} ${FILE}
}

ConfigureHS() {
  # Default HS_IP to RM_IP if not defined and if it's the first time running this section
  # It will only set HS_IP to RM_IP IF __HS_IP__ is found in mapred-site.xml
  if [ -z "${HS_IP}" -a `grep "__HS_IP__" "${HADOOP2_HOME}/etc/hadoop/mapred-site.xml" | wc -l` -ne 0 ]; then
    logInfo "No IP/hostname provided for History Server. Will be configured to 0.0.0.0"
    HS_IP="0.0.0.0"
  fi

  # Set history server IP in yarn-site.xml
  if [ ! -z "$HS_IP" ]; then
    FILENAME="mapred-site"
    FILE="${HADOOP2_HOME}/etc/hadoop/${FILENAME}.xml"
    TMPL="${FILE}.template"
    # Check if old file has HS set already. If it does, then back it up
    if [ `grep "__HS_IP__" $FILE | wc -l` -eq 0 ]; then
        TIMESTAMP=$(date +%F.%H-%M)
        logInfo "Backing up \"$HADOOP2_HOME/etc/hadoop/mapred-site.xml\" to \"$HADOOP2_HOME/etc/hadoop/mapred-site-${TIMESTAMP}.xml\""
        cp ${FILE} $HADOOP2_HOME/etc/hadoop/${FILENAME}-${TIMESTAMP}.xml
    fi
    # Replace HS_IP from template file and redirect output to new file
    sed  "s/__HS_IP__/${HS_IP}/g" "${TMPL}" > "${FILE}"
  fi
}

IsRMHAConfiguration() {
  local rmIPs=$1
  local prevIFS=$IFS
  set -- "$rmIPs" 
  IFS=","; declare -a RM_IP_ARRAY=($*);IFS=$prevIFS
  if [ ${#RM_IP_ARRAY[@]} -eq 1 ]; then
    return 1
  fi
  return 0
}

ConfigureYarnServices() {
  maprHA=0
  # Set Resource Manager IP
  if [ ! -z "$1" ]; then
    RM_IP="$1"
  else
    maprHA=1
  fi

# Configure RM Service in Warden to run only on a single node or multiple nodes depending on
# whether MapR RM HA is configured or not.
  WardenRMConfFile=${MAPR_HOME}/conf/conf.d/warden.resourcemanager.conf
  if [ -e ${WardenRMConfFile} ]; then
    if [ $maprHA -eq 1 ]; then
      runOnNodes=1
    else
      runOnNodes=all
    fi
    sed -i -e "s/^services=resourcemanager:.*:cldb$/services=resourcemanager:${runOnNodes}:cldb/" ${WardenRMConfFile}
  fi

  if [ ! -z "$2" ]; then
      HS_IP="$2"
  fi

  if [ $maprHA -eq 1 ]; then
     logInfo "No RM addresses were provided. Will configure MapR HA for Resource Manager.."
     ConfigureYarnSiteXml com.mapr.hadoop.yarn.configuration.YarnSiteMapRHAXmlBuilder
  else
     IsRMHAConfiguration $RM_IP
     if [ $? -eq 0 ]; then
       logInfo "Multiple IPs/hostnames are provided for Resource Manager. Will configure high availability (HA) for Resource Manager.."
       ConfigureYarnSiteXml com.mapr.hadoop.yarn.configuration.YarnHASiteXmlBuilder ${RM_IP} $clusterName $zkNodesList
     else
        ConfigureYarnSiteXml com.mapr.hadoop.yarn.configuration.YarnSiteXmlBuilder ${RM_IP}
     fi
  fi

  MAPR_SECURITY_STATUS=$(head -n 1 /opt/mapr/conf/mapr-clusters.conf | grep secure= | sed 's/^.*secure=//' | sed 's/ .*$//')
  if [ "$MAPR_SECURITY_STATUS" = "true" ]; then
     ConfigureYarnSiteXml com.mapr.hadoop.yarn.configuration.YarnSiteAclXmlBuilder
  fi

  ConfigureHS
}

ConfigureTimeLineServer() {
  local WardenTLConfFile="${MAPR_HOME}/conf/conf.d/warden.timelineserver.conf"
  local WardenTLConfFileTmpl="${HADOOP2_HOME}/etc/hadoop/warden.timelineserver.conf"
  local YarnSiteFile="${HADOOP2_HOME}/etc/hadoop/yarn-site.xml"
  local YarnTLProps="${HADOOP2_HOME}/etc/hadoop/yarn-timelineserver-properties.xml"
  local YarnTLSecurityProps="${HADOOP2_HOME}/etc/hadoop/yarn-timelineserver-security-properties.xml"
  local YSTIMESTAMP=$(date +%F.%H-%M)
  local yarnSiteChange=0

  if [ -f ${FILE} ]; then
    logInfo "Backing up \"$HADOOP2_HOME/etc/hadoop/yarn-site.xml\" to \"$HADOOP2_HOME/etc/hadoop/yarn-site-${YSTIMESTAMP}.xml\""
    cp ${YarnSiteFile} $HADOOP2_HOME/etc/hadoop/yarn-site-${YSTIMESTAMP}.xml
  fi

  #add timeline-server properties to yarn-site.xml
  if !(grep -Fxq "<!--TIMELINE SERVER SECTION-->" ${YarnSiteFile}); then
      yarnSiteChange=1
      sed -i -e "/<\/configuration>/ {
              r ${YarnTLProps}
              d}" ${YarnSiteFile}
      echo "</configuration>" >> ${YarnSiteFile}
  fi
  if (grep -Fxq "<!--TIMELINE SERVER SECTION-->" ${YarnSiteFile}); then
      sed -i -e "/<name>yarn.timeline-service.hostname<\/name>/!b;n;c\ \ \ \ <value>$1<\/value>" ${YarnSiteFile}
  fi
  if [ "$isSecure" = "true" ];  then
      if !(grep -Fxq "<!--TIMELINE SECURITY SECTION-->" ${YarnSiteFile}); then
          yarnSiteChange=1
          sed -i -e "/<\/configuration>/ {
                  r ${YarnTLSecurityProps}
                  d}" ${YarnSiteFile}
          echo "</configuration>" >> ${YarnSiteFile}
      fi
  fi

  if [ "$isSecure" = "false" ];  then
      if (grep -Fxq "<!--TIMELINE SECURITY SECTION-->" ${YarnSiteFile}); then
          yarnSiteChange=1
          sed -i -e '/<!--TIMELINE SECURITY SECTION-->/,/<!--TIMELINE SECURITY SECTION END-->/d' ${YarnSiteFile}
      fi
  fi

  if [ $yarnSiteChange -eq 1 ] && ${MAPR_HOME}/initscripts/mapr-warden status > /dev/null 2>&1; then
      if ! [ -d "${RESTART_DIR}" ]; then
          mkdir -p "${RESTART_DIR}"
      fi
      if [ -f $ROLES/resourcemanager ]; then
          if ! [ -f  "${RESTART_DIR}/resourcemanager_restart.sh" ]; then
              cat > "$RM_RESTART_FILE"  <<-RM_RESTART
                  echo "Running RM restart script"
                  if ${MAPR_HOME}/initscripts/mapr-warden status > /dev/null 2>&1 ; then
                      isSecure=$(head -1 ${MAPR_HOME}/conf/mapr-clusters.conf | grep -o 'secure=\w*' | cut -d= -f2)
                      if [ "$isSecure" = "true" ] && [ -f "${MAPR_HOME}/conf/mapruserticket" ]; then
                          export MAPR_TICKETFILE_LOCATION="${MAPR_HOME}/conf/mapruserticket"
                      fi
                      nohup maprcli node services -name resourcemanager -action restart -nodes $(hostname -f) > ${RESTART_LOG_DIR}/rm_restart_$(date +%s)_$$.log 2>&1 &
                  fi
RM_RESTART
              chmod +x "$RM_RESTART_FILE"
          fi
      fi
      if [ -f $ROLES/timelineserver ]; then
          if ! [ -f  "${RESTART_DIR}/timelineserver_restart.sh" ]; then
              cat > "$TL_RESTART_FILE"  <<-TL_RESTART
                  echo "Running TL restart script"
                  if ${MAPR_HOME}/initscripts/mapr-warden status > /dev/null 2>&1 ; then
                      isSecure=$(head -1 ${MAPR_HOME}/conf/mapr-clusters.conf | grep -o 'secure=\w*' | cut -d= -f2)
                      if [ "$isSecure" = "true" ] && [ -f "${MAPR_HOME}/conf/mapruserticket" ]; then
                          export MAPR_TICKETFILE_LOCATION="${MAPR_HOME}/conf/mapruserticket"
                      fi
                      nohup maprcli node services -name timelineserver -action restart -nodes $(hostname -f) > ${RESTART_LOG_DIR}/tl_restart_$(date +%s)_$$.log 2>&1 &
                  fi
TL_RESTART
              chmod +x "$TL_RESTART_FILE"
          fi
      fi
  fi


  if [ ! -f ${WardenTLConfFile} -a -f ${ROLES}/timelineserver ]; then
    cp $WardenTLConfFileTmpl "${MAPR_HOME}/conf/conf.d"
  fi
}

ConfigureHadoop2() {
  sed -i -e 's|{MAPR_HOME}|'/opt/mapr'|g' "$HADOOP2_HOME"/etc/hadoop/ssl-client.xml
  sed -i -e 's|{MAPR_HOME}|'/opt/mapr'|g' "$HADOOP2_HOME"/etc/hadoop/ssl-server.xml

  ln -sf "$HADOOP2_HOME"/etc/hadoop/ssl-client.xml /opt/mapr/conf/ssl-client.xml
  ln -sf "$HADOOP2_HOME"/etc/hadoop/ssl-server.xml /opt/mapr/conf/ssl-server.xml
}


ConfigureYarnLinuxContainerExecutor() {
  # Only configure container executor for yarn
  # Set the MapR specific values in container-executor.cfg
  FILENAME="container-executor.cfg"
  FILE=${HADOOP2_HOME}/etc/hadoop/${FILENAME}
  sed -i -e "s/^\(yarn\.nodemanager\.linux-container-executor\.group\)=#.*$/\1=${MAPR_GROUP}/" ${FILE}
  sed -i -e "s/^\(min\.user\.id\)=1000#.*$/\1=500/" ${FILE}
  sed -i -e "s/^\(allowed\.system\.users\)=#.*$/\1=${MAPR_USER}/" ${FILE}

  # Change ownership and mode for container-executor binary.
  chown root:${MAPR_GROUP} ${HADOOP2_HOME}/bin/container-executor
  chmod 6050 ${HADOOP2_HOME}/bin/container-executor
}

cleanUpOldMapRfsJars()
{
    local ITEM
    local LATEST=""

    for ITEM in $(find  $HADOOP_HOME/lib -regextype posix-extended -regex "$1" -print 2> /dev/null); do
        if [ -z "$LATEST" ]; then
            LATEST="$ITEM"
        elif [ "$ITEM" -nt "$LATEST" ]; then
            rm -f "$LATEST"
            LATEST="$ITEM"
        elif [ "$ITEM" -ot "$LATEST" ]; then
            rm -f "$ITEM"
        fi
    done
}

ConfigureHadoopMain() {
  # Process arguments
  if [ -z "$1" ]; then
    if [ "$default_mode" = "classic" ]; then
      HADOOP=1
    elif [ "$default_mode" = "yarn" ]; then
      HADOOP=2
    fi
  else
    HADOOP="$1"
  fi

  if [ -z "$2" ]; then
    if [ "$default_mode" = "classic" ]; then
      HADOOP_VERSION=$classic_version
    elif [ "$default_mode" = "yarn" ]; then
      HADOOP_VERSION=$yarn_version
    fi
  else
    HADOOP_VERSION="$2"
  fi

  HADOOP2_HOME=/opt/mapr/hadoop/hadoop-"$yarn_version"
  HADOOP_CORE_HOME=$HADOOP2_HOME

  HADOOP_HOME=/opt/mapr/hadoop/hadoop-"$HADOOP_VERSION"

  echo "Configuring Hadoop-"$HADOOP_VERSION" at "$HADOOP_HOME""
  ConfigureCommon

  ConfigureHadoop2

  # Update the default hadoop version variable and in file
  default_hadoop_version=$HADOOP_VERSION
  default_hadoop=$HADOOP

  echo "Done configuring Hadoop"
}

function ConfigureMyriadInternal() {
 m_cluster_name=$1
 mf_name=$2

 myriadVersion=$(ls -lt ${INSTALL_DIR}/myriad | grep "myriad-" | head -1 | sed 's/^.*myriad-//' | awk '{print $1}')

 MYRIAD_DIR="${INSTALL_DIR}/myriad/myriad-${myriadVersion}"

 myriadConf="${MYRIAD_DIR}/conf/myriad-config-default.yml"

 mkdir -p "${MYRIAD_DIR}/conf/conf.old"

 logInfo "Updating Myriad config"
 now=`date +%Y-%m-%d.%H-%M`
 tmpMyriadConf="/tmp/myriad-config-default.yml.$now"
 cp -Rp $myriadConf "${MYRIAD_DIR}/conf/conf.old/myriad-config-default.yml"."$now"

 # copy the myriad conf file to a temporary file and make all changes in the temporary file
 cat $myriadConf > $tmpMyriadConf

 ### Configure myriad-config-default.yml
 sed -i -e "s/mesosMaster: .*$/mesosMaster: zk:\/\/${zkNodesList}\/mesos/" $tmpMyriadConf
 if [ ! -z "$mf_name" ]; then
  sed -i -e "s/frameworkName: .*$/frameworkName: ${mf_name}/" $tmpMyriadConf
 fi
 sed -i -e "s/frameworkUser: .*$/frameworkUser: ${MAPR_USER}/" $tmpMyriadConf
 sed -i -e "s/zkServers: .*$/zkServers: ${zkNodesList}/" $tmpMyriadConf

 # set Myriad HA flag
 if [ "$mHA" = "y" ]; then
   sed -i -e "s/haEnabled: .*$/haEnabled: true/" $tmpMyriadConf
 else
   sed -i -e "s/haEnabled: .*$/haEnabled: false/" $tmpMyriadConf
 fi

 # find this file first and then replace
 executorFile=$(ls $MYRIAD_DIR/lib/myriad-executor-runnable*)
 if [ $? -eq 0 ]; then
  sed -i -e "s|path: .*$|path: file://$executorFile|" $tmpMyriadConf
 fi
 sed -i -e "s|YARN_HOME: .*$|YARN_HOME: ${hadoopBase}/hadoop-${hadoopVersion}|" $tmpMyriadConf
 # need to be careful not to overwrite manual options
 if [ ! -z "$m_cluster_name" ]; then
  optsExist=$(grep YARN_NODEMANAGER_OPTS $tmpMyriadConf)
  if [ $? -eq 0 ]; then
   # check if cluster.name.prefix exist - if yes and not the same replace
   # if no add at the end of the line
   clusterPropExist=$(echo "$optsExist" | grep cluster.name.prefix)
   if [ $? -eq 0 ]; then
     # check if it is the same
     isSame=$(echo "$clusterPropExist" | grep "\-Dcluster.name.prefix=/$m_cluster_name")
     if [ $? -ne 0 ]; then
      # not the same - change
       sed -i -e "s|-Dcluster.name.prefix=/\S*|-Dcluster.name.prefix=/$m_cluster_name |" $tmpMyriadConf
     fi
   else
    # does not exist - add as first option
    sed -i -e "s/YARN_NODEMANAGER_OPTS: /YARN_NODEMANAGER_OPTS: -Dcluster.name.prefix=\/$m_cluster_name /" $tmpMyriadConf
   fi
  else
   # add YARN_NODEMANAGER_OPTS
   sed -i -e '/yarnEnvironment:/a\  YARN_NODEMANAGER_OPTS: -Dcluster.name.prefix=/'${m_cluster_name}'' $tmpMyriadConf
  fi
 fi
  # configure JHS
  isAux=$(grep "services:\|jobhistory:" $tmpMyriadConf)
  if [ $? -ne 0 ]; then
    echo "services:" >> $tmpMyriadConf
    ConfigureMyriadJHSAdd $tmpMyriadConf
  else
    isJHS=$(echo $isAux | grep "jobhistory:")
    if [ $? -ne 0 ]; then
      # need to configure jhs only after services:
      ConfigureMyriadJHSInsert $tmpMyriadConf
    fi
  fi

 #move the temporary myriad conf file to myriad-config-default.yml
 mv $tmpMyriadConf $myriadConf
}

function ConfigureMyriadJHSAdd() {
  echo "   jobhistory:" >> $1
  echo "     jvmMaxMemoryMB: 64" >> $1
  echo "     cpus: 0.5" >> $1
  echo "     ports:" >> $1
  echo "       myriad.mapreduce.jobhistory.admin.address: 10033" >> $1
  echo "       myriad.mapreduce.jobhistory.address: 10020" >> $1
  echo "       myriad.mapreduce.jobhistory.webapp.address: 19888" >> $1

  if [ ! -z "$m_cluster_name" ]; then
    echo "     envSettings: -Dcluster.name.prefix=/${m_cluster_name}" >> $1
  else
    echo "     envSettings:"  >> $1
  fi
  echo "     taskName: jobhistory" >> $1
  echo "     serviceOptsName: HADOOP_JOB_HISTORYSERVER_OPTS" >> $1
  echo "     command: \$YARN_HOME/bin/mapred historyserver" >> $1
  echo "     maxInstances: 1" >> $1
}

function ConfigureMyriadJHSInsert() {
  envSet=""
  if [ ! -z "$m_cluster_name" ]; then
    envSet="-Dcluster.name.prefix=/${m_cluster_name}"
  fi

  sed -i -e '/services:/a\   jobhistory:\
     jvmMaxMemoryMB: 64\
     cpus: 0.5\
     ports: \
       myriad.mapreduce.jobhistory.admin.address: 10033\
       myriad.mapreduce.jobhistory.address: 10020\
       myriad.mapreduce.jobhistory.webapp.address: 19888\
     envSettings: '${envSet}'\
     taskName: jobhistory\
     serviceOptsName: HADOOP_JOB_HISTORYSERVER_OPTS\
     command: $YARN_HOME/bin/mapred historyserver\
     maxInstances: 1' $1
}

