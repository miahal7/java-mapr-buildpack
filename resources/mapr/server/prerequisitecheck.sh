#!/bin/bash
# Copyright (c) 2009 & onwards. MapR Tech, Inc., All rights reserved

# This utility will check all the requirements to install MapR.

PORTS="5660 7221 7222 5181 9001 50030 50060 8443 60000 60010 60020 60030"
INSTALL_DIR=${MAPR_HOME:-/opt/mapr}
env=${INSTALL_DIR}/conf/env.sh
[ -f $env ] && . $env


if [ ! -z $JAVA_HOME ]; then
 export PATH=$JAVA_HOME/bin:$PATH
fi

CURRENT_USER=`whoami`
logFile="${INSTALL_DIR}/logs/prerequisitecheck-${CURRENT_USER}.log"

function logInfo() {
  echo INFO:$* >> $logFile
}

function logErr() {
  echo ERROR:$* >> $logFile
  echo ERROR:$* 
}

function logWarn() {
  echo WARNING:$* >> $logFile
  echo WARNING:$*
}


function CheckTheJavaVersion() {
  inCompat=0
          
  if [ -n "$JAVA_HOME" -a -d "$JAVA_HOME" ]; then
   if [ -f "$JAVA_HOME"/bin/java ]; then
    JAVA="$JAVA_HOME"/bin/java
   fi
   elif [ "n$JAVA_HOME" = "n" ]; then
    which java > /dev/null 2> /dev/null
    if [ $? = 0  ]; then
     JAVA=java
    fi
  fi

  #
  # Verify that the recommended Java vendor and version are in $PATH
  # 
  if [ "$JAVA" = "" ]; then
    logErr "There is no Java available either in default PATH: $PATH or through JAVA_HOME environment variable"
    inCompat=1
  else
   $JAVA -version > /dev/null 2>&1
   if [ $? != 0 ]; then
    logErr "There is no Java available either in default PATH: $PATH or through JAVA_HOME environment variable"
    inCompat=1
   else
    logInfo "Version of Java:"
    $JAVA -version >> $logFile 2>&1
    logInfo ""

    distOk=0
    verOk=0
    bitOk=0

    $JAVA -version 2>&1 | egrep -e "^(Java|OpenJDK)" > /dev/null
    if [ $? -eq 0 ]; then
      distOk=1
    fi

    $JAVA -version 2>&1 | grep "version" | egrep -e "1.(7|8)" > /dev/null
    if [ $? -eq 0 ]; then
      verOk=1
    fi

    $JAVA -version 2>&1 | grep -i "64-Bit" > /dev/null
    if [ $? -eq 0 -o "$isClient" == "1" ]; then
      bitOk=1
    fi

    if [ $distOk -eq 0 -o $verOk -eq 0 -o $bitOk -eq 0 ]; then
      logErr " $JAVA found via JAVA_HOME or on PATH is not 64-Bit SUN-Java 1.7.x or 1.8.x, or 64-Bit OpenJDK 1.7.x or 1.8.x. [${distOk}${verOk}${bitOk}]"
      inCompat=1
    fi
   fi
  fi
  
  if [ $inCompat == 1 ]; then
    logErr " MapR recommends 64-Bit SUN-Java 1.7.x or 1.8.x, or 64-Bit OpenJDK 1.7.x or 1.8.x."
    logErr " ACTION: Install a recommended version of Java and set the JAVA_HOME environment variable in env.sh." 
    actionReqd=1
  fi
}

function CheckTheOSVersion () {
  logInfo "OS version of the current machine:"
  cat /etc/issue >> $logFile
  logInfo ""
  cat /etc/issue 2>/dev/null | egrep "Red Hat.*release 6|Red Hat.*release 7|Ubuntu 12.04|Ubuntu 14.04|CentOS release 6|CentOS release 7"  > /dev/null
  if [ $? == 0 ]; then
    return;
  fi

  logWarn "MapR is not supported for this OS "
  logWarn "ACTION: MapR supported OS versions: RedHat 6.x, RedHat 7.x, Centos 6.x, Centos 7.x, Ubuntu 12.04 and Ubuntu 14.04 "
  actionReqd=1
}

function CheckThePorts() {
  for port in $PORTS
  do
    netstat -an | grep ":$port " | grep "LISTEN " > /dev/null 2>&1
    if [ $? == 0 ]; then
      logWarn "ACTION: MapR requires the port '$port' which is already in use. Free that port if not used by MapR already. "
      actionReqd=1
    fi
  done
}

########
# main #
########

echo "Checking the MapR requirements" > $logFile

while [ $# -gt 0 ]
do
  case "$1" in
    -isClient)
      isClient=1;;
  esac
  shift
done

actionReqd=0
touch $logFile;
chmod 600 $logFile;

CheckTheJavaVersion
#CheckTheOSVersion
#CheckThePorts


if [ $actionReqd == 0 ]; then
  logInfo "OS and Java versions are MapR compatible in the current machine. Go Ahead with MapR installation"
else
  logWarn "There is some incompatibility in the current machine to install MapR. Check the above ACTION items to resolve them"
fi
