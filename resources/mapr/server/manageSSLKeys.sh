#!/bin/bash
# Copyright (c) 2013 & onwards. MapR Tech, Inc., All rights reserved

#TODO    Need to find java the same way as configure.sh

#This script creates the default key and trust stores used by MapR for
#various HTTPS connections. These are self signed. If you wish, you can
#manually replace the contents of the ssl_keystore and ssl_truststore with
#CA issued certificates if you prefer.

INSTALL_DIR=${MAPR_HOME:=/opt/mapr}

sslKeyStore=${INSTALL_DIR}/conf/ssl_keystore
sslTrustStore=${INSTALL_DIR}/conf/ssl_truststore
storePass=mapr123
storeFormat=JKS
expireInDays="36500"
#VERBOSE="-v"

. ${INSTALL_DIR}/server/scripts-common.sh
if [ "$JAVA_HOME"x = "x" ]; then
  KEYTOOL=`which keytool`
else
  KEYTOOL=$JAVA_HOME/bin/keytool
fi

# Check if keytool is actually valid and exists
if [ ! -e "${KEYTOOL:-}" ]; then
    echo "The keytool in \"${KEYTOOL}\" does not exist."
    echo "Keytool not found or JAVA_HOME not set properly. Please install keytool or set JAVA_HOME properly."
    exit 1
fi

CLUSTERNAME="my.cluster.com"

#discover DNS domain for host unless provided on command line and use
#in certificate DN
DOMAINNAME=`hostname -d`
if [ "$DOMAINNAME"x = "x" ]; then
  CERTNAME=`cat ${INSTALL_DIR}/hostname`
else
  CERTNAME="*."$DOMAINNAME
fi


function confirmNotThere() {
  if [ -f $sslKeyStore ]; then
    echo "$sslKeyStore already exists"
    exit 1
  fi

  if [ -f $sslTrustStore ]; then
    echo "$sslTrustStore already exists"
    exit 1
  fi
}

function createCertificates() {
  #create self signed certificate with private key

  echo "Creating 100 year self signed certificate with subjectDN='CN=$CERTNAME'"
  $KEYTOOL -genkeypair -sigalg SHA512withRSA -keyalg RSA -alias $CLUSTERNAME -dname CN=$CERTNAME -validity $expireInDays \
           -storepass $storePass -keypass $storePass \
           -keystore $sslKeyStore -storetype $storeFormat $VERBOSE
  if [ $? -ne 0 ]; then
	echo "Keytool command to generate key store failed"
  fi

  #extract self signed certificate into trust store
  tfile=/tmp/tmpfile-mapcert.$$
  /bin/rm -f $tfile
  $KEYTOOL -exportcert -keystore $sslKeyStore -file $tfile \
           -alias $CLUSTERNAME -storepass $storePass -storetype $storeFormat $VERBOSE
  if [ $? -ne 0 ]; then
	echo "Keytool command to extract certificate from key store failed"
  fi
  $KEYTOOL -importcert -keystore $sslTrustStore -file $tfile \
            -alias $CLUSTERNAME -storepass $storePass -noprompt $VERBOSE
  if [ $? -ne 0 ]; then
	echo "Keytool command to create trust store failed"
  fi
  /bin/rm -f $tfile
}

function setPermissions() {
  #set permissions on key and trust store
  chown $MAPR_UG $sslKeyStore $sslTrustStore
  chmod u=r,go= $sslKeyStore
  chmod ugo=r $sslTrustStore
}

function  merge() {
  from=$1
  to=$2
  echo "Merging certificates from $from into existing $to"
  if [ ! -f "$from" ]; then
    echo "Source trust store not found: $from"
    exit 1
  fi
  if [ ! -f "$to" ]; then
    echo "Destination trust store not found: $to"
    exit 1
  fi
  $KEYTOOL -importkeystore -srckeystore $from -destkeystore $to \
	-deststorepass $storePass -srcstorepass $storePass $VERBOSE 
  if [ $? -ne 0 ]; then
	echo "Keytool command to merge two trust stores failed"
  fi
}

function usage() {
  echo ""
  echo "manageSSLKeys.sh is a tool to create and manage the SSL certificates."
  echo "it is run once on the first node from configure.sh"
  echo "Usage: manageSSLKeys and one of  "
  echo "       create [-d DNSDOMAIN] [-N clustername]"
  echo "              creates the SSL key and trust stores needed for HTTPS traffic"
  echo "              -d specifies DNS domain used in wildcard certificate. Default"
  echo "                 is detected from Local OS"
  echo "              -N clustername"
  echo "              -ug MapR user/group, e.g., mapr:mapr"
  echo "       merge <in trust store> <out trust store>"
  echo "             merges the certificates from the in trust store into the existing out trust store"

}

################
#  main        #
################
command=$1
shift

if [ "$command" = "merge" ]; then
    from=$1
    to=$2
    merge $from $to
elif [ "$command" = "create" ]; then
    while [ $# -gt 0 ]; do
      case "$1" in
      -d) shift;
          CERTNAME="*."$1;;
      -N) shift;
          CLUSTERNAME=$1;;
      -ug) shift;
          MAPR_UG=$1;;
      *) usage;
          exit 1;;
      esac
      shift
    done
  confirmNotThere
  createCertificates
  setPermissions
else
  usage
  exit 1
fi

exit

confirmNotThere
createCertificates
setPermissions
