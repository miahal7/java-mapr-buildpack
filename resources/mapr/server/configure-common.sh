#!/bin/bash
# Copyright (c) 2009 & onwards. MapR Tech, Inc., All rights reserved

hadoopVersion=
hadoopConfDir=

INSTALL_DIR=${MAPR_HOME:=/opt/mapr}

# Source scripts-common
. ${INSTALL_DIR}/server/scripts-common.sh

# Source hadoop version variables
. ${INSTALL_DIR}/conf/hadoop_version

# This file contains functions to configure MAPR_USER to run the services
# this file will be included in both configure and upgrade scripts

MAPR_NOFILES_ULIMIT=${MAPR_NOFILES_ULIMIT:=65536}
MAPR_NPROC_ULIMIT=${MAPR_NPROC_ULIMIT:=64000}

function ReadRoles() {
 if [ ! -d "$ROLES" ]; then
     #
     # If the clientOnly flag has been given, then this message is redundant.
     #
     if [ $clientOnly == 0 ]; then
         echo "INFO: No $ROLES directory was found. Node will be configured as client only."
     fi 
     return
 fi
 roles=`ls "$ROLES" | tr " " "\n"`
 rolesCount=$(echo $roles | wc -w)
 if [ $rolesCount -eq 0 ]; then
   echo "INFO: No roles were defined in $ROLES. Node will be configured as client only."
 #  clientOnly=1
   return
 fi
 echo "Node setup configuration: " $roles
 echo "Log can be found at: "  $logFile

 for x in $roles
 do
  case "$x" in
    zookeeper) zkNodeOn=1;;
    fileserver) fsNodeOn=1;;
    cldb) cldbNodeOn=1;;
    nfs) nfsNodeOn=1;;
    oozie) oozieNodeOn=1;;
    webserver) wsNodeOn=1;;
    drill-qs) drillQSNodeOn=1;;
    drill-internal) drillInternalNodeOn=1;;
    drill-yarn) drillYarnNodeOn=1;;
    drill-bits) drillBitsNodeOn=1;;
    asynchbase) asynchbNodeOn=1;;
    hbinternal) hbINodeOn=1;;
    impalaserver) impalaServerRoleOn=1;;
    mapr-mesos-dns) 
         mesosDnsOn=1
         mesosOn=1;;
    mapr-mesos-master) mesosOn=1;;
    mapr-mesos-slave) mesosOn=1;;
    mapr-mesos-marathon) 
         marathonOn=1
         mesosOn=1;;
    mapr-myriad-executor) myriadOn=1;;
    mapr-myriad-scheduler) myriadOn=1;;
    elasticsearch) esNodeOn=1;;
    opentsdb) otNodeOn=1;;
    fluentd) fdNodeOn=1;;
    collectd) cdNodeOn=1;;
    grafana) gdNodeOn=1;;
    kibana) kdNodeOn=1;;
    nfs4) nfs4NodeOn=1;;
    apiserver) apiServerNodeOn=1;;
  esac
 done
}

function GetZKConfigPath() {
  if [ -f "${INSTALL_DIR}/zookeeper/zookeeperversion" ]; then
    ver=$(cat "${INSTALL_DIR}/zookeeper/zookeeperversion")
  else 
    ver=$(ls -lt ${INSTALL_DIR}/zookeeper | grep "zookeeper-" | head -1 | sed 's/^.*zookeeper-//' | awk '{print $1}')
  fi
  zk_config="${ZK_INSTALL_DIR}/zookeeper-${ver}/conf/zoo.cfg" 
}

function  ConfigureRunUserForZKRole() {
  if [ "$MAPR_USER" = "root" ]; then
    logInfo "MAPR_USER=root, no user changes needed to run ZooKeeper"
    return;
  fi

  logInfo "Config MAPR_USER for ZooKeeper Role"
  # Zookeeper when running as mapr user, needs ownership of the zkdata folder
  # and the zookeeper installation directory.
  chown -R $MAPR_USER "${ZK_INSTALL_DIR}" >> $logFile 2>&1
  chown -R $MAPR_USER "${dataDir}" >> $logFile 2>&1
  chgrp -R $MAPR_GROUP "${ZK_INSTALL_DIR}" >> $logFile 2>&1
  chgrp -R $MAPR_GROUP "${dataDir}" >> $logFile 2>&1
}

function ConfigureHive() {
  if [ "$MAPR_USER" = "root" ]; then
    return;
  fi
  if [ $clientOnly -eq 0 ]; then
   if [ -d  "${INSTALL_DIR}/hive" ]; then
     ver=$(ls -lt ${INSTALL_DIR}/hive | grep "hive-" | head -1 | sed 's/^.*hive-//' | awk '{print $1}')
     chown -R $MAPR_USER "${INSTALL_DIR}/hive/hive-${ver}" >> $logFile 2>&1
   fi
  fi
}

function ConfigureRunUserForHadoopInternal() {
  HADOOP_DIR="${INSTALL_DIR}/hadoop/hadoop-${2}"
  if [ ! -d $HADOOP_DIR ]; then
    logWarn "Hadoop directory does not exist: $HADOOP_DIR"
    return
  fi

  CURR_USER=$1
  [ -d "${HADOOP_DIR}/logs" ] && chown $CURR_USER "${HADOOP_DIR}/logs" >> $logFile 2>&1
  [ -d "${HADOOP_DIR}/logs" ] && [ "$(ls -A ${HADOOP_DIR}/logs)" ] && chown $CURR_USER "${HADOOP_DIR}/logs/"* >> $logFile 2>&1
  [ -d "${HADOOP_DIR}/pids" ] && chown -R $CURR_USER "${HADOOP_DIR}/pids" >> $logFile 2>&1
  [ -d "${HADOOP_DIR}/conf" ] && chown -R $CURR_USER "${HADOOP_DIR}/conf" >> $logFile 2>&1
  [ -d "${HADOOP_DIR}/conf.new" ] && chown -R $CURR_USER "${HADOOP_DIR}/conf.new" >> $logFile 2>&1
  [ -d "${HADOOP_DIR}/etc/hadoop" ] &&  find "${HADOOP_DIR}/etc/hadoop" -type f | grep -v container-executor.cfg | xargs chown  $CURR_USER  >> $logFile 2>&1
}

function ConfigureRunUserForHadoop() {

  # Configure for Hadoop 2

  ConfigureRunUserForHadoopInternal $1 $yarn_version

}

function ConfigureRunUserForJM() {
  BIN_DIR="${INSTALL_DIR}/bin"
  if [ ! -d $BIN_DIR ]; then
    return
  fi

  SETUP_SQL="${BIN_DIR}/setup.sql"
  if [ ! -f $SETUP_SQL ]; then
    return
  fi

  CURR_USER=$1

  # set uid=mapr on the whole /opt/mapr/bin dir
  # so that central config can create a backup file for setup.sql
  chown -R $CURR_USER "${BIN_DIR}" >> $logFile 2>&1
}

# expects two args: 
#   arg1 - full path to hbase dir
#   arg2 - user to set the owner to
function ConfigureRunUserForHbase() {
  HBASE_DIR=$1 # for e.g /opt/mapr/hbase/hbase-0.90.4
  if [ ! -d $HBASE_DIR/conf ]; then
    return
  fi

  CURR_USER=$2
  chown -R $CURR_USER "${HBASE_DIR}/conf" >> $logFile 2>&1
}

function ConfigureRunUserForDrill() {
  DRILL_DIR=$1
  if [ ! -d $DRILL_DIR/conf ]; then
    return
  fi

  CURR_USER=$2
  chown -R $CURR_USER "${DRILL_DIR}/conf" >> $logFile 2>&1
}

function ConfigureRunUserTTLocalDir() {
  if [ "$MAPR_USER" = "root" ]; then
    echo "MAPR_USER=root, skip changes to TTLocalDir" >> $logFile 2>&1
    return;
  fi

  # get mapred.local.dir from mapred-site and change permissions on the dir
  TT_MAPRED_LOCAL_DIR=$(hadoop conf 2> /dev/null | grep "<name>mapred\.local\.dir</name>" | awk -F'</?value>' '{print $2}')
  # Only modify files if TT_MAPRED_LOCAL_DIR is found
  if [ ! -z "${TT_MAPRED_LOCAL_DIR}" ]; then

    # Readlink to find if any links are in the given directory
    links=`readlink -f $TT_MAPRED_LOCAL_DIR`
    # Should return either blank if directory does not exist, or the same directory path if it does exist
    if [[ ! -z $links && $links != $TT_MAPRED_LOCAL_DIR ]]; then
      # Print out if logfile exists.
      if [ -f $logFile ]; then
          echo "Found the following files in $TT_MAPRED_LOCAL_DIR that are symbolic links: $links. Please remove them before proceeding. Exiting..." >> $logFile 
      fi
      echo "Found the following files in $TT_MAPRED_LOCAL_DIR that are symbolic links: $links. Please remove them before proceeding. Exiting..."
      # Use exit in case this is being run from warden
      declare -f ExitSingleInstance > /dev/null; 
      if [ $? -eq 0 ]; then
          ExitSingleInstance 1
      else 
          exit 1
      fi

    fi

    CURR_USER=$1
    CURR_GROUP=$2
    #mkdir -p creates entire directory tree but does not honor the mode
    #for each directory created. So instead, fiddle with the umask
    um=`umask`
    umask u=rwx,go=rx
    mkdir -p $TT_MAPRED_LOCAL_DIR
    umask $um
    chown $CURR_USER $TT_MAPRED_LOCAL_DIR >> $logFile 2>&1
    chgrp $CURR_GROUP $TT_MAPRED_LOCAL_DIR >> $logFile 2>&1

  fi
}

function ChownServiceLogs() {
  if [ ! -z "$(find /opt/mapr/logs -maxdepth 1 -name "cldb*")" ]; then
    chown $MAPR_USER ${INSTALL_DIR}/logs/cldb* >> $logFile 2>&1
  fi
  if [ ! -z "$(find /opt/mapr/logs -maxdepth 1 -name "hoststats*")" ]; then
    chown $MAPR_USER ${INSTALL_DIR}/logs/hoststats* >> $logFile 2>&1
  fi
  if [ ! -z "$(find /opt/mapr/logs -maxdepth 1 -name "mfs*")" ]; then
    chown $MAPR_USER ${INSTALL_DIR}/logs/mfs* >> $logFile 2>&1
  fi
  if [ ! -z "$(find /opt/mapr/logs -maxdepth 1 -name "nfs*")" ]; then
    chown $MAPR_USER ${INSTALL_DIR}/logs/nfs* >> $logFile 2>&1
  fi
  if [ ! -z "$(find /opt/mapr/logs -maxdepth 1 -name "warden*")" ]; then
    chown $MAPR_USER ${INSTALL_DIR}/logs/warden* >> $logFile 2>&1
  fi
}

function ConfigureRunUserForWS() {
  local val=`getent group shadow 2>/dev/null`
  if [ "$?" != "0" -o "$val" = "" ]; then
    # Create a group named shadow
    groupadd shadow  >> $logFile 2>&1
  fi
  # Add CURR_USER to the group wheel
  if [ -f /etc/SuSE-release ]; then
      # Add CURR_USER to the group wheel for SUSE
      usermod -A shadow $MAPR_USER >> $logFile 2>&1
      STATUS=$?
      #
      # The '-A' option has been removed from SuSE 12 
      #
      if [ $STATUS -ne 0 ]; then
          usermod -a -G shadow $MAPR_USER  >> $logFile 2>&1
      fi
  else
      usermod -a -G $MAPR_GROUP,shadow $MAPR_USER  >> $logFile 2>&1
  fi
  # Change group-owner of /etc/shadow
  chgrp shadow /etc/shadow  >> $logFile 2>&1
  # Allow read permissions for user shadow
  chmod ug+r /etc/shadow >> $logFile 2>&1
}

function ConfigureRunUserMaprDirs() {
  if [ "$MAPR_USER" = "root" ]; then
    logInfo "MAPR_USER=root, no user changes needed in logs/conf dirs"
    return;
  fi

  logInfo "Config MAPR_USER for logs/conf of MapR Services"
  mkdir -p "${INSTALL_DIR}/logs"
  mkdir -p "${INSTALL_DIR}/conf/conf.d"

  ChownRootFiles "${INSTALL_DIR}/server/data" $MAPR_USER $MAPR_GROUP
  ChownRootFiles "${INSTALL_DIR}/logs" $MAPR_USER $MAPR_GROUP
  ChownNoUserFiles "${INSTALL_DIR}/logs" $MAPR_USER $MAPR_GROUP
  ChownServiceLogs
  # we don't want to mess with the permissions in the proxy dir
  find "${INSTALL_DIR}/conf" -path "${INSTALL_DIR}/conf/proxy" \
      -prune -o -exec chown $MAPR_USER {} \; >> $logFile 2>&1
  chgrp -R $MAPR_GROUP "${INSTALL_DIR}/conf" >> $logFile 2>&1
}


function SetPermissionOnOoozieExec() {
  CURR_GROUP=$1
  chgrp $CURR_GROUP $INSTALL_DIR/server/oozieexecute >> $logFile 2>&1
  chmod 4750 $INSTALL_DIR/server/oozieexecute >> $logFile 2>&1
}

function ConfigureRunUserForOozieRole() {
  if [ "$MAPR_USER" = "root" ]; then
    logInfo "MAPR_USER=root, no user changes needed to run Oozie"
    return;
  fi

  mkdir -p "${OOZIE_DIR}/logs"
  mkdir -p "${OOZIE_DIR}/oozie-server/temp/"
  mkdir -p "${OOZIE_DIR}/oozie-server/webapps/"
  mkdir -p "${OOZIE_DIR}/oozie-server/logs/"
  mkdir -p "${OOZIE_DIR}/oozie-server/work/"
  mkdir -p "${OOZIE_DIR}/data/"
  mkdir -p "${OOZIE_DIR}/pids"

  chown -R $MAPR_USER:$MAPR_GROUP  "${OOZIE_DIR}" >> $logFile 2>&1

  SetPermissionOnOoozieExec $MAPR_GROUP
}

function ConfigureUserForMesosRoles() {
  if [ "$MAPR_USER" = "root" ]; then
    logInfo "MAPR_USER=root, no user changes needed to run Mesos"
    return;
  fi
  chown -R $MAPR_USER ${MESOS_DIR}/logs
  chmod a+w ${MESOS_DIR}/logs
  # to work around issue with permissions in mesos 0.25
  chmod 755 /usr/bin/mesos-init-wrapper
  chmod 644 /etc/default/mesos*
  chown -R $MAPR_USER ${MESOS_DIR}/conf
  chown $MAPR_USER /var/log/mesos
  chown $MAPR_USER /var/lib/mesos
  if [ "$marathonOn" -eq 1 ]; then
    chmod 755 /usr/bin/marathon
  fi
}

function ConfigureRunUserForMonitoringRoles() {
  if [ "$MAPR_USER" = "root" ]; then
    logInfo "MAPR_USER=root, no user changes needed to run Monitoring services"
    return;
  fi

  chown -R $MAPR_USER:$MAPR_GROUP  "${MONITORING_DIR}" >> $logFile 2>&1
}

function create_user() {
  # Create MAPR_USER
  SERVER_NAME="MapR Daemon"

  # create user to avoid running server as root
  # create group if not existing
  if ! getent group | grep -q "^$MAPR_GROUP:" ; then
    logInfo "Adding group $MAPR_GROUP.."
    AG_OPTIONS="-r --gid $maprGroupId"
    echo "groupadd $AG_OPTIONS $MAPR_GROUP" >> $logFile 2>&1
    groupadd $AG_OPTIONS $MAPR_GROUP >> $logFile 2>&1
    err=$?
    if [ $err -ne 0 ]; then
      logInfo "group add failed $err"
      echo "groupadd failed."
      return
    fi
    DELETE_GROUP=1
  fi

  # create user if not existing
  if ! getent passwd | grep -q "^$MAPR_USER:"; then
    logInfo "Adding user $MAPR_USER.."
    AU_OPTIONS="-g $MAPR_GROUP -r --uid $maprUserId"
    useradd -h 2>&1 | grep "\-M" > /dev/null
    if [ $? -eq 0 ]; then
      # use -M if supported
      AU_OPTIONS="$AU_OPTIONS -M"
    fi

    echo "useradd $AU_OPTIONS $MAPR_USER" >> $logFile 2>&1
    useradd $AU_OPTIONS $MAPR_USER >> $logFile 2>&1
    err=$?
    if [ $err -ne 0 ]; then
      logInfo "useradd failed $err"
      echo "useradd failed."
      return
    fi
    DELETE_USER=1
  fi

  # adjust passwd entry
  usermod -c "$SERVER_NAME" -g $MAPR_GROUP $MAPR_USER >> $logFile 2>&1
}

function ConfigMaprUser() {
  DELETE_USER=0
  DELETE_GROUP=0

  logInfo "MAPR_USER: $MAPR_USER MAPR_GROUP: $MAPR_GROUP"
  logInfo "CREATE_USER: $CREATE_USER"
  logInfo "maprUserId: $maprUserId maprGroupId: $maprGroupId"

  if [ -z "$MAPR_USER" ]; then
    logErr "MAPR_USER is not specified, use -u option"
    echo "MAPR_USER is not specified, use -u option"
    ExitSingleInstance 1
  fi

  DAEMON_CONF="${INSTALL_DIR}/conf/daemon.conf"
  if [ -e $DAEMON_CONF ]; then
    PREV_MAPR_USER=$( awk -F = '$1 == "mapr.daemon.user" { print $2 }' $DAEMON_CONF)
    if [ "xx$PREV_MAPR_USER" != "xx$MAPR_USER" ]; then
      # Clean up if the PREV_MAPR_USER wasn't the same user
      DAEMON_CONF_BKUP="${INSTALL_DIR}/conf/daemon.conf.old"
      CLEANUP_MAPRUSER_SCRIPT="${INSTALL_DIR}/server/cleanup-maprconf"
      echo "run $CLEANUP_MAPRUSER_SCRIPT" >> $logFile
      $CLEANUP_MAPRUSER_SCRIPT >> $logFile 2>&1
      echo "save old $DAEMON_CONF" >> $logFile
      mv $DAEMON_CONF $DAEMON_CONF_BKUP >> $logFile 2>&1
    fi
  fi
  

  [ -z $MAPR_GROUP ] && MAPR_GROUP=$MAPR_USER
  if [ "$MAPR_USER" = "root" ]; then
    logInfo "MAPR_USER=root, no changes needed in limits.conf"
    echo "mapr.daemon.user=$MAPR_USER" > $DAEMON_CONF
    echo "mapr.daemon.group=$MAPR_GROUP" >> $DAEMON_CONF
    #ensure proper permissions for daemon.conf
    chmod go+r $DAEMON_CONF
    return;
  fi

  [ -z $CREATE_USER ] && CREATE_USER=0

  id $MAPR_USER >> $logFile 2>&1
  err=$?
  if [ $err -ne 0 ]; then
    if [ $CREATE_USER -eq 1 ]; then
      [ -z $maprUserId ] && maprUserId=$[0x7ffffff0]
      [ -z $maprGroupId ] && maprGroupId="$maprUserId"
  
      create_user
      id $MAPR_USER >> $logFile 2>&1
      err=$?
      if [ $err -ne 0 ]; then
        logErr "id MAPR_USER failed error=$err, create_user failed."
        echo "User/Group creation failed, log can be found at: $logFile"
        echo -n "uid:$maprUserId gid:$maprGroupId or both might be in use. "
        echo -n "Please use an available uid/gid using -U and -G options. "
        echo    "Note: Same uid/gid should be used across the cluster."
        ExitSingleInstance 1
      else
        echo "id $MAPR_USER is created, please run 'passwd $MAPR_USER' to set a password for $MAPR_USER."
      fi
    else
      logErr "id MAPR_USER failed error=$err, $MAPR_USER does not exists"
      echo "No such user $MAPR_USER. Create the user or use --create-user option"
      ExitSingleInstance 1
    fi
  else
    if [ $CREATE_USER -eq 1 ]; then
      logErr "User:$MAPR_USER already exists, exiting with error"
      echo "User:$MAPR_USER already exists, cannot create user."
      ExitSingleInstance 1
    else
      if [ "xx$maprUserId" != "xx" -o "xx$maprGroupId" != "xx" ]; then
        logErr "-U/-G options used with out --create-user, exiting with error"
        echo "Invalid options, -U/-G should be used only with --create-user or -a"
        ExitSingleInstance 1
      fi
    fi
  fi

  logInfo "Give privilleges to $MAPR_USER"
  chgrp ${MAPR_GROUP} ${INSTALL_DIR}/server/maprexecute >> $logFile 2>&1
  chmod 4750 ${INSTALL_DIR}/server/maprexecute >> $logFile 2>&1

  chgrp ${MAPR_GROUP} ${INSTALL_DIR}/server/manageSSLKeys.sh >> $logFile 2>&1
  chmod 755 ${INSTALL_DIR}/server/manageSSLKeys.sh >> $logFile 2>&1

  mkdir -p "${INSTALL_DIR}/mapr-cli-audit-log" >> $logFile 2>&1
  chown ${MAPR_USER} ${INSTALL_DIR}/mapr-cli-audit-log >> $logFile 2>&1
  chgrp ${MAPR_GROUP} ${INSTALL_DIR}/mapr-cli-audit-log >> $logFile 2>&1
  chmod 700 ${INSTALL_DIR}/mapr-cli-audit-log >> $logFile 2>&1
  chown ${MAPR_USER} ${INSTALL_DIR}/server/cliauditlogger >> $logFile 2>&1
  chgrp ${MAPR_GROUP} ${INSTALL_DIR}/server/cliauditlogger >> $logFile 2>&1
  chmod 6711 ${INSTALL_DIR}/server/cliauditlogger >> $logFile 2>&1

  ConfigureRunUserMaprDirs

  DAEMON_CONF="${INSTALL_DIR}/conf/daemon.conf"
  logInfo "Update $DAEMON_CONF"
  # set mapr user name if not there yet
  grep "mapr.daemon.user" $DAEMON_CONF > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "mapr.daemon.user=$MAPR_USER" >> $DAEMON_CONF
  fi

  #ensure proper permissions for daemon.conf
  chmod go+r $DAEMON_CONF
  chown root $DAEMON_CONF
  chgrp root $DAEMON_CONF

  # set mapr group name if not there yet
  grep "mapr.daemon.group" $DAEMON_CONF > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    echo "mapr.daemon.group=$MAPR_GROUP" >> $DAEMON_CONF
  fi

  if [ "$DELETE_GROUP" -eq "1" ]; then
    sed -i -e '/mapr.uninstall.delete.group/d' $DAEMON_CONF > /dev/null 2>&1
    echo "mapr.uninstall.delete.group=1" >> $DAEMON_CONF
  fi
  if [ "$DELETE_USER" -eq "1" ]; then
    sed -i -e '/mapr.uninstall.delete.user/d' $DAEMON_CONF > /dev/null 2>&1
    echo "mapr.uninstall.delete.user=1" >> $DAEMON_CONF
  fi

  LIMITSCONF="/etc/security/limits.conf"

  logInfo "set $MAPR_USER limits in $LIMITSCONF"
  # TODO remove later
  sed -i -e '/'"${MAPR_USER}"' soft memlock/d' $LIMITSCONF
  sed -i -e '/'"${MAPR_USER}"' hard memlock/d' $LIMITSCONF
  sed -i -e '/'"${MAPR_USER}"' soft core/d' $LIMITSCONF
  sed -i -e '/'"${MAPR_USER}"' hard core/d' $LIMITSCONF

  #remove any existing entries and set unilimted memlock/core for MAPR_USER
  #needed for mfs daemon
  sed -i -e '/'"${MAPR_USER}"' - memlock/d' $LIMITSCONF
  echo "${MAPR_USER} - memlock unlimited"  >> $LIMITSCONF
  sed -i -e '/'"${MAPR_USER}"' - core/d' $LIMITSCONF
  echo "${MAPR_USER} - core unlimited"  >> $LIMITSCONF
  nfiles=$(cat $LIMITSCONF | fgrep "$MAPR_USER - nofile" | cut -d' ' -f4)
  if [ -n "$nfiles" ]; then
    if [ "$nfiles" != "unlimited" ] && [ "$nfiles" -lt "${MAPR_NOFILES_ULIMIT}" ]; then
      sed -i -e '/'"${MAPR_USER}"' - nofile/d' $LIMITSCONF
      echo "${MAPR_USER} - nofile ${MAPR_NOFILES_ULIMIT}"  >> $LIMITSCONF
    fi
  else
    echo "${MAPR_USER} - nofile ${MAPR_NOFILES_ULIMIT}"  >> $LIMITSCONF
  fi

  # neeeded for CLDB
  nproc=$(cat $LIMITSCONF | fgrep "$MAPR_USER - nproc" | cut -d' ' -f4)
  if [ -n "$nproc" ]; then
    if [ "$nproc" != "unlimited" ] && [ "$nproc" -lt "${MAPR_NPROC_ULIMIT}" ]; then
      sed -i -e '/'"${MAPR_USER}"' - nproc/d' $LIMITSCONF
      echo "${MAPR_USER} - nproc ${MAPR_NPROC_ULIMIT}"  >> $LIMITSCONF
    fi
  else
    echo "${MAPR_USER} - nproc ${MAPR_NPROC_ULIMIT}"  >> $LIMITSCONF
  fi

  #needed for zookeeper/warden
  sed -i -e '/'"${MAPR_USER}"' - nice/d' $LIMITSCONF
  echo "${MAPR_USER} - nice -10"  >> $LIMITSCONF

  # setup core generation
  if [ -f /proc/sys/kernel/core_pattern ]; then
    coresDir=$(dirname "$(cat /proc/sys/kernel/core_pattern)")
  else
    coresDir="."
  fi
  if [ ${coresDir:0:1} == "|" ] || [ ${coresDir:0:1} == "." ] || [ $coresDir == "/" ]; then
    echo "/opt/cores/%e.core.%p.%h" > /proc/sys/kernel/core_pattern
    coresDir="/opt/cores"
  fi
  mkdir -p $coresDir
  chmod 1777 $coresDir

  # change permissions for all conf files to be -x
  for confDir in `find "${MAPR_HOME:-}" -type d -name conf`; do
    for confFile in "${confDir}"/*; do
      if [ -f "${confFile:-}" ]; then
	   chmod a-x "${confFile:-}" >> $logFile 2>&1
      fi
    done
  done

  logInfo "$MAPR_USER/$MAPR_GROUP user/group configured"
}

function ConfigureWardenToRunAsMaprUser() {
  if [ "$MAPR_USER" = "root" ]; then
    logInfo "MAPR_USER=root, no user changes needed to run warden"
    return;
  fi

  DAEMON_CONF="${INSTALL_DIR}/conf/daemon.conf"
  sed -i -e '/mapr.daemon.runuser.warden/d' $DAEMON_CONF > /dev/null 2>&1
  echo "mapr.daemon.runuser.warden=1" >> $DAEMON_CONF
}

function ChownRootFiles() {
  dir=$1
  CURR_USER=$2
  CURR_GROUP=$3

  files=`find $dir -user root`

  for f in $files
  do
    chown $CURR_USER $f >> $logFile 2>&1
    chgrp $CURR_GROUP $f >> $logFile 2>&1
  done
}

function ChownNoUserFiles() {
  dir=$1
  CURR_USER=$2
  CURR_GROUP=$3

  ufiles=`find $dir -nouser`
  gfiles=`find $dir -nogroup`
  files="$ufiles $gfiles"

  for f in $files
  do
    chown $CURR_USER $f >> $logFile 2>&1
    chgrp $CURR_GROUP $f >> $logFile 2>&1
  done
}

function SetAioMaxNr() {
  if [ $(id -u) -ne 0 ]; then
    local errMsg="Must be root to set fs.aio-max-nr"
    echo $errMsg;
    logErr $errMsg;
    return
  fi

  local curMax=$(cat /proc/sys/fs/aio-max-nr)
  local reqMax=$(expr 16 \* 16 \* 1024)
  if [ $curMax -lt $reqMax ]; then
    echo "fs.aio-max-nr=$reqMax" >> /etc/sysctl.conf
    sysctl -p > /dev/null
  fi
}

# The variables $yarn_version, etc. are obtained from hadoop_versions file
# that is sourced in this script.
function setHadoopVersion() {
  hadoop=$1

  if [ -z "$hadoop" ]; then
    if [ "$default_mode" = "classic" ]; then
      errMsg="Classic Hadoop configuration no longer supported"
      echo $errMsg;
      logErr $errMsg;
      exit 1
    elif [ "$default_mode" = "yarn" ]; then
      hadoop=2
    fi
  fi

  if [ "$hadoop" == "1" ]; then
    errMsg="Classic Hadoop configuration no longer supported"
    echo $errMsg;
    logErr $errMsg;
    exit 1
  elif [ "$hadoop" == "2" ]; then
    hadoopVersion="${yarn_version}"
  else
    errMsg="Unknown Hadoop option - $hadoop"
    echo $errMsg;
    logErr $errMsg;
    exit 1
  fi
}

function setHadoopConfDir() {
  hadoop=$1
  hadoopHomeDir=$2

  if [ -z "$hadoop" ]; then
    hadoopConfDir="${hadoopHomeDir}/conf"
  elif [ "$hadoop" == "1" ]; then
    hadoopConfDir="${hadoopHomeDir}/conf"
  elif [ "$hadoop" == "2" ]; then
    hadoopConfDir="${hadoopHomeDir}/etc/hadoop"
  else
    errMsg="Unknown Hadoop option - $hadoop"
    echo $errMsg;
    logErr $errMsg;
    exit 1
  fi
}

function getRealMacID() {
  # logname returns real user id - even under sudo
  MAPR_USER=$( logname )
  MAPR_GROUP=$( id -gn $MAPR_USER )
}

function checkMacID() {
  local rc=0
  # idType is group or user
  local idType=$1
  local idToCheck=$2
  dscacheutil -q $idType | fgrep name | fgrep $idToCheck 2>&1 > /dev/null
  if [ $? -ne 0 ]; then
    errMsg="ERROR: Make sure $idType $idToCheck exist"
    echo $errMsg;
    logErr $errMsg;
    rc=1
  fi
  return $rc
}

function verifyMacUserExist() {
  local rcuser=0
  local rcgroup=0
  #check to see if MAPR_USER exist
  checkMacID user $MAPR_USER
  rcuser=$?
  #check to see if MAPR_GROUP exist
  checkMacID group $MAPR_GROUP
  rcgroup=$?
  if [ $rcgroup -ne 0 -o $rcuser -ne 0 ]; then
    exit 1
  fi
}
