/**
* Licensed to the Apache Software Foundation (ASF) under one
* or more contributor license agreements. See the NOTICE file
* distributed with this work for additional information
* regarding copyright ownership. The ASF licenses this file
* to you under the Apache License, Version 2.0 (the
* "License"); you may not use this file except in compliance
* with the License. You may obtain a copy of the License at
*
* http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
#ifndef LIBHBASE_TYPES_H_
#define LIBHBASE_TYPES_H_

#include <stddef.h>
#include <stdint.h>
#ifndef _MSC_VER
#include <stdbool.h>
#endif
#include <string.h>
#include <errno.h>

#include "macros.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * HBase ubiquitous byte type
 */
#ifdef byte_t
#undef byte_t
#endif
typedef uint8_t byte_t;

/**
 * Base HBase Cell type.
 */
typedef struct hb_cell_type {
  /* row key */
  byte_t *row;
  size_t  row_len;

  /* column family */
  byte_t *family;
  size_t  family_len;

  /* column qualifier */
  byte_t *qualifier;
  size_t  qualifier_len;

  /* column value */
  byte_t *value;
  size_t  value_len;

  /* timestamp */
  int64_t ts;

  /* For internal use, applications should not set or alter this variable. */
  int64_t flags_;
  void   *private_;
} hb_cell_t;

#define HBASE_LATEST_TIMESTAMP  0x7fffffffffffffffL

/**
 * Inline function to initialize a hb_cell_t.
 */
inline int32_t
hb_cell_t_init(hb_cell_t *cell) {
  if (cell) {
    memset(cell, 0, sizeof(hb_cell_t));
    cell->ts = HBASE_LATEST_TIMESTAMP;
    return 0;
  }
  return EINVAL;
}

typedef void *hb_connection_t;
typedef void *hb_client_t;
typedef void *hb_admin_t;

typedef void *hb_table_t;
typedef void *hb_get_t;
typedef void *hb_result_t;
typedef void *hb_mutation_t;
typedef void *hb_put_t;
typedef void *hb_delete_t;
typedef void *hb_increment_t;
typedef void *hb_append_t;
typedef void *hb_scanner_t;
typedef void *hb_columndesc;
typedef void *hb_repl_stats_t;
typedef void *hb_repl_desc_iter_t;
typedef void *hb_repl_desc_t;
typedef void *hb_repl_column_t;

typedef enum {
  DURABILITY_USE_DEFAULT = 0, /* Use column family's default setting. */
  DURABILITY_SKIP_WAL    = 1, /* Do not write the Mutation to the WAL */
  DURABILITY_ASYNC_WAL   = 2, /* Write the Mutation to the WAL asynchronously. */
  DURABILITY_SYNC_WAL    = 3, /* Write the Mutation to the WAL synchronously. */
  DURABILITY_FSYNC_WAL   = 4  /* Write the Mutation to the WAL synchronously and force
                               * the entries to disk. (Note: this is currently not
                               * supported and will behave identical to SYNC_WAL).
                               * See https://issues.apache.org/jira/browse/HADOOP-6313 */
} hb_durability_t;

/**
 * Replication state
 */
typedef enum {
  COPY_IN_PROGRESS = 0,
  REPLICATING      = 1
} hb_replication_state_t;

/**
 * Replication on-wire compression type. Default: lz4
 */
typedef enum {
    off = 0,
    lzf = 2,
    lz4 = 3,
    zlib = 4
} hb_compression_type_t;

/**
 *  Log levels
 */
typedef enum {
  HBASE_LOG_LEVEL_INVALID = 0,
  HBASE_LOG_LEVEL_FATAL   = 1,
  HBASE_LOG_LEVEL_ERROR   = 2,
  HBASE_LOG_LEVEL_WARN    = 3,
  HBASE_LOG_LEVEL_INFO    = 4,
  HBASE_LOG_LEVEL_DEBUG   = 5,
  HBASE_LOG_LEVEL_TRACE   = 6
} HBaseLogLevel;

/*******************************************************************************
 *                            HBase Error Codes                                *
 *******************************************************************************
 * The HBase APIs returns or invokes the callback functions with the error     *
 * code set to 0. A non-zero value indicates an error condition.               *
 *                                                                             *
 * EAGAIN   A recoverable error occurred and the operation can be immediately  *
 *          retired.                                                           *
 *                                                                             *
 * ENOBUFS  The resources required to complete the operations are temporarily  *
 *          exhausted and the application should retry the operation after a   *
 *          brief pause, possibly throttling the rate of requests.             *
 *                                                                             *
 * ENOENT   The requested table or a column family was not found.              *
 *                                                                             *
 * EEXIST   Table or column family already exist.                              *
 *                                                                             *
 * ENOMEM   The process ran out of memory while trying to process the request. *
 *                                                                             *
 * HBASE_TABLE_DISABLED                                                        *
 *          The table is disabled.                                             *
 *                                                                             *
 * HBASE_TABLE_NOT_DISABLED                                                    *
 *          The table is not disabled.                                         *
 *                                                                             *
 * HBASE_UNKNOWN_SCANNER                                                       *
 *          The specified scanner does not exist on region server.             *
 *                                                                             *
 * HBASE_INTERNAL_ERR                                                          *
 *          An internal error for which there is no error code defined. More   *
 *          information will be available in the log.                          *
 *                                                                             *
 *******************************************************************************/
/**
 * HBase custom error codes
 */
#define HBASE_INTERNAL_ERR                    -10000
#define HBASE_TABLE_DISABLED                  (HBASE_INTERNAL_ERR-1)
#define HBASE_TABLE_NOT_DISABLED              (HBASE_INTERNAL_ERR-2)
#define HBASE_UNKNOWN_SCANNER                 (HBASE_INTERNAL_ERR-3)
#define HBASE_REPL_SRC_TABLE_NOT_FOUND        (HBASE_INTERNAL_ERR-20)
#define HBASE_REPL_REPLICA_TABLE_NOT_FOUND    (HBASE_INTERNAL_ERR-21)
#define HBASE_REPL_UPSTREAM_NOT_FOUND         (HBASE_INTERNAL_ERR-22)
#define HBASE_REPL_REPLICA_NOT_FOUND          (HBASE_INTERNAL_ERR-23)
#define HBASE_REPL_NEW_REPLICA_TABLE_NOT_FOUND (HBASE_INTERNAL_ERR-24)
#define HBASE_REPL_CF_NOT_FOUND               (HBASE_INTERNAL_ERR-25)
#define HBASE_REPL_REPLICA_EXIST              (HBASE_INTERNAL_ERR-26)
#define HBASE_REPL_UPSTREAM_EXIST             (HBASE_INTERNAL_ERR-27)
#define HBASE_REPL_LICENSE_ERROR              (HBASE_INTERNAL_ERR-28)
#define HBASE_REPL_REPLICA_SAME               (HBASE_INTERNAL_ERR-29)
#define HBASE_REPL_BULKLOAD_TRUE              (HBASE_INTERNAL_ERR-30)
#define HBASE_REPL_OP_OUTSTANDING             (HBASE_INTERNAL_ERR-31)
#define HBASE_REPL_PERMISSION_ERROR           (HBASE_INTERNAL_ERR-32)
#define HBASE_REPL_REPLICA_DIR_NOT_EXIST      (HBASE_INTERNAL_ERR-33)
#define HBASE_REPL_MULTI_MASTER_WITH_EXISTING_REPLICA  (HBASE_INTERNAL_ERR-34)
#define HBASE_REPL_DIRECTCOPY_NOT_ENABLED     (HBASE_INTERNAL_ERR-35)

/*******************************************************************************
 *                            HBase API Callbacks                              *
 *******************************************************************************
 *      The following section defines various callback functions for the async *
 * APIs.  These callbacks may be triggered on the same thread as the caller or *
 * on another thread. The application is expected to not block in the callback *
 * routines.                                                                   *
 *******************************************************************************/

/**
 * HBase Admin disconnection callback typedef
 *
 * This callback is triggered after the connections are closed, but just before
 * the client is freed.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_admin_disconnection_cb)(
    int32_t err,
    hb_admin_t admin,
    void *extra);

/**
 * Client disconnection callback typedef
 * This callback is triggered after the connections are closed, but just before
 * the client is freed.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_client_disconnection_cb) (
      int32_t err,
      hb_client_t client,
      void *extra);

/**
 * Client flush callback typedef
 * This callback is invoked when everything that was buffered at the time of
 * the call has been flushed.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_client_flush_cb) (
      int32_t err,
      hb_client_t client,
      void *extra);

/**
 * Mutation callback typedef
 *
 * This callback is triggered with the result of each mutation.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_mutation_cb)(
      int32_t err,
      hb_client_t client,
      hb_mutation_t mutation,
      hb_result_t result,
      void *extra);

/**
 * Get callback typedef
 * This callback is triggered with the result of each Get call.
 *
 * The result, if not NULL, must be freed by calling hb_result_destroy().
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_get_cb)(
      int32_t err,
      hb_client_t client,
      hb_get_t get,
      hb_result_t result,
      void *extra);


/**
 * Scanner callback typedef
 * This callback is triggered when scanner next returns results.
 *
 * The individual results in the results array must be freed by calling
 * hb_result_destroy(). The array itself is freed once the callback returns.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_scanner_cb)(
      int32_t err,
      hb_scanner_t scanner,
      hb_result_t results[],
      size_t num_results,
      void *extra);

/**
 * Scanner close callback typedef
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_scanner_destroy_cb)(
      int32_t err,
      hb_scanner_t scanner,
      void *extra);

/**
 * Replication add replica callback typedef
 * This callback is triggered when replica add finishes.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_add_replica_cb)(
        int32_t err,
        hb_admin_t admin,
        hb_repl_desc_t rp,
        void *extra);

/**
 * Replication add upstream callback typedef
 * This callback is triggered when upstream add finishes.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_add_upstream_cb)(
        int32_t err,
        hb_admin_t admin,
        const char* src,
        const char* replica,
        void *extra);

/**
 * Replication remove replica callback typedef
 * This callback is triggered when replica remove finishes.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_remove_replica_cb)(
        int32_t err,
        hb_admin_t admin,
        const char* src,
        const char* replica,
        void *extra);

/**
 * Replication remove upstream callback typedef
 * This callback is triggered when upstream remove finishes.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_remove_upstream_cb)(
        int32_t err,
        hb_admin_t admin,
        const char* src,
        const char* replica,
        void *extra);

/**
 * Replication edit replica callback typedef
 * This callback is triggered when replica edit finishes.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_edit_replica_cb)(
        int32_t err,
        hb_admin_t admin,
        hb_repl_desc_t rp,
        void *extra);

/**
 * Replication pause replica callback typedef
 * This callback is triggered when replication pause finishes.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_pause_replica_cb)(
        int32_t err,
        hb_admin_t admin,
        const char* src,
        const char* replica,
        void *extra);

/**
 * Replication resume replica callback typedef
 * This callback is triggered when replication resume finishes.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_resume_replica_cb)(
        int32_t err,
        hb_admin_t admin,
        const char* src,
        const char* replica,
        void *extra);

/**
 * Replication list replica callback typedef
 * This callback is triggered when replica list finishes
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_list_replica_cb)(
        int32_t err,
        hb_admin_t admin,
        const char* src,
        hb_repl_desc_iter_t it,
        bool refresh,
        void *extra);

/**
 * Replication list upstream callback typedef
 * This callback is triggered when upstream list finishes.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_list_upstream_cb)(
        int32_t err,
        hb_admin_t admin,
        const char* replica,
        hb_repl_desc_iter_t it,
        void *extra);

/**
 * Replication autosetup callback typedef
 * This callback is triggered when autosetup finishes.
 *
 * Refer to the section on error code for the list of possible values
 * for 'err'. A value of 0 indicates success.
 */
typedef void (*hb_repl_auto_setup_cb)(
        int32_t err,
        hb_admin_t admin,
        hb_repl_desc_t rp,
        void *extra);

#ifdef __cplusplus
}  /* extern "C" */
#endif

#endif /* LIBHBASE_TYPES_H_ */
