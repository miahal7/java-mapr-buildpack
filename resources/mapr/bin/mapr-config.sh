#!/bin/bash
# Copyright (c) 2009 & onwards. MapR Tech, Inc., All rights reserved
#
# This script configures various settings for MapR scripts
#

this="${BASH_SOURCE-$0}"
while [ -h "$this" ]; do
  ls=`ls -ld "$this"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '.*/.*' > /dev/null; then
    this="$link"
  else
    this=`dirname "$this"`/"$link"
  fi
done

# Convert relative path to absolute path
bin=`dirname "$this"`
script=`basename "$this"`
home=`cd "$bin/..">/dev/null; pwd`
this="$home/bin/$script"

# Test for cygwin
IS_CYGWIN=false
case "`uname`" in
CYGWIN*) IS_CYGWIN=true;;
esac

# Root of the MapR installation
if [ -z "$MAPR_HOME" ]; then
  MAPR_HOME="$home"
fi

# Root of the Hadoop installation
if [ -z "$HADOOP_HOME" ]; then
  if [ -f ${MAPR_HOME}/hadoop/hadoopversion ]; then
    HADOOP_VERSION=`cat ${MAPR_HOME}/hadoop/hadoopversion`
    HADOOP_HOME="${MAPR_HOME}/hadoop/hadoop-${HADOOP_VERSION}"
  fi
fi

# Various MapR folders
MAPR_BIN=$MAPR_HOME/bin
MAPR_CONF=$MAPR_HOME/conf
MAPR_LIB=$MAPR_HOME/lib
MAPR_LOGS=$MAPR_HOME/logs

# Source the env.sh to set various environment variables
. $MAPR_CONF/env.sh

# Few environment variables used by loggers
MAPR_PROCESS_NAME=${MAPR_PROCESS_NAME:=$(basename ${BASH_SOURCE[1]})} # parent script name
MAPR_PROCESS_USER=`id -nu`
MAPR_PROCESS_USERID=`id -u`

# Prints files matching the specified pattern from the
# specified folder separated by PATH separator ':'
#
# $1:       Folder to include files from
# $2[...]:  Files (wildcards allowed) from the folder.
#           If you use wildcards, make sure to "quote" them.
get_files_in_folder() {
  FOLDER=$1
  shift

  PATH_FILES=
  until [ -z "$1" ]; do
    FOUND_FILES=`echo $(ls ${FOLDER}/$1 2> /dev/null) | sed -E 's/[[:space:]]+/:/g'`
    if [ "${FOUND_FILES}" != "" ]; then
      PATH_FILES=${PATH_FILES}:${FOUND_FILES}
    fi
    shift
  done
  # Remove any additional ':' from the tail
  PATH_FILES="${PATH_FILES#:}"
  echo $PATH_FILES
}

#MapR core jars
get_mapr_core_jars() {
  get_files_in_folder $MAPR_LIB \
    "maprfs-*.jar"\
    "maprdb-*.jar"\
    "mapr-hbase-*.jar"\
    "mapr-ojai-driver*.jar"\
    "mapr-streams-*.jar"\
    "mapr-tools-*.jar"
}

#MapR client jars
get_mapr_client_jars() {
  MAPRCLIENT_CLASSPATH=$MAPR_CONF
  MAPRCLIENT_CLASSPATH=$MAPRCLIENT_CLASSPATH:$(get_mapr_core_jars)
  MAPRCLIENT_CLASSPATH=$MAPRCLIENT_CLASSPATH:$(get_logger_jars)
  MAPRCLIENT_CLASSPATH=$MAPRCLIENT_CLASSPATH:$(get_drill_jars)
  MAPRCLIENT_CLASSPATH=$MAPRCLIENT_CLASSPATH:$(get_external_jars)
  echo $MAPRCLIENT_CLASSPATH
}

# Add Apache Drill jars
get_drill_jars() {
if [ -n ${DRILL_HOME} ]; then
  if [ -f ${MAPR_HOME}/drill/drillversion ]; then
    DRILL_VERSION=`cat ${MAPR_HOME}/drill/drillversion`
    DRILL_HOME="${MAPR_HOME}/drill/drill-${DRILL_VERSION}"
  fi
fi
if [ -z ${DRILL_HOME} ]; then
  (>&2 echo "Warning: Unable to determine \$DRILL_HOME")
  return
fi

DRILL_CORE_JARS=$(get_files_in_folder ${DRILL_HOME}/jars\
    "drill-auth-mechanism-maprsasl-*.jar"\
    "drill-common-*.jar"\
    "drill-java-exec-*.jar"\
    "drill-logical-*.jar"\
    "drill-memory-base-*.jar"\
    "drill-protocol-*.jar"\
    "drill-rpc-*.jar"\
    "vector-*.jar")
DRILL_3P_JARS=$(get_files_in_folder ${DRILL_HOME}/jars/3rdparty\
    "antlr4-runtime-4*.jar"\
    "curator-*.jar"\
    "commons-lang3*.jar"\
    "config-1*.jar"\
    "guava-*.jar"\
    "hppc-*.jar"\
    "javassist-*.jar"\
    "metrics-*-3*.jar"\
    "netty-*-4*.jar"\
    "xmlenc-*.jar")
DRILL_CLASSB_JARS=$(get_files_in_folder ${DRILL_HOME}/jars/classb\
    "reflections-*.jar")

echo $DRILL_CORE_JARS:$DRILL_3P_JARS:$DRILL_CLASSB_JARS
}

# Add 3rd party libraries
get_external_jars() {
  MAPR_LIB_JARS=$(get_files_in_folder $MAPR_LIB\
    "antlr4-runtime-*.jar"\
    "commons-logging-1.*.jar"\
    "commons-lang-*.jar"\
    "commons-configuration-*.jar"\
    "commons-collections-*.jar"\
    "hadoop-common-*.jar"\
    "hadoop-auth-*.jar"\
    "guava-*.jar"\
    "jackson-annotations-*.jar"\
    "jackson-core-*.jar"\
    "jackson-databind-*.jar"\
    "jline-*.jar"\
    "json-[1-9].[0-9].jar"\
    "kafka-clients*.jar"\
    "ojai*.jar"\
    "protobuf*.jar"\
    "trove4j*.jar"\
    "zookeeper-*.jar")
  HADOOP_LIB_JARS=$(get_files_in_folder $HADOOP_HOME/share/hadoop/common/lib\
    "commons-cli-*.jar"\
    "commons-codec-*.jar"\
    "commons-io-*.jar")
echo $MAPR_LIB_JARS:$HADOOP_LIB_JARS
}

# Add Spring farmework libraries needed by dbshell
get_spring_jars() {
  get_files_in_folder $MAPR_LIB\
    "spring-asm-*.jar"\
    "spring-beans-*.jar"\
    "spring-context-*.jar"\
    "spring-core-*.jar"\
    "spring-expression-*.jar"\
    "spring-shell-*.jar"
}

# Add logging libraries
get_logger_jars() {
  get_files_in_folder $MAPR_LIB\
    "slf4j-api-*.jar"\
    "slf4j-log4j12-*.jar"\
    "log4j-*.jar"\
    "central-logging-*.jar"
}

get_hadoop_classpath() {
  HADOOP_IN_PATH=$(PATH="${HADOOP_HOME:-${HADOOP_PREFIX}}/bin:$PATH" which hadoop 2>/dev/null)
  if [ -f ${HADOOP_IN_PATH} ]; then
    ${HADOOP_IN_PATH} classpath 2>/dev/null
  fi
}

get_hadoop_libpath() {
  HADOOP_IN_PATH=$(PATH="${HADOOP_HOME:-${HADOOP_PREFIX}}/bin:$PATH" which hadoop 2>/dev/null)
  if [ -f ${HADOOP_IN_PATH} ]; then
    ${HADOOP_IN_PATH} jnipath 2>/dev/null
  fi
}

export IS_CYGWIN
export MAPR_HOME
export MAPR_BIN
export MAPR_CONF
export MAPR_LIB
export MAPR_LOGS
export MAPR_PROCESS_NAME
export MAPR_PROCESS_USER
export MAPR_PROCESS_USERID
export HADOOP_HOME
